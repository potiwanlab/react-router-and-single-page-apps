import { createRef, useState } from "react";

const Create = ({skill, setSkill}) => {
  const itemInput = createRef();

  const handleAddItem = () => {
    const item = itemInput.current.value;
    // alert("Hello, " + item);
    setSkill([
        ...skill,
        item
    ])
  };

  return (
    <>
      <div>
        <h1>Create</h1>
        ToDo : <input ref={itemInput} />
        <button onClick={handleAddItem}>Add Item</button>

        <p>
            Skills : {JSON.stringify(skill)}
        </p>

      </div>
    </>
  );
};

export default Create;
