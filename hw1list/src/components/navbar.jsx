import { Link } from "react-router-dom"

const Navbar = () => {
    return (
        <ui>
            <li>
                <Link to="/">Home</Link>
            </li>
            <li>
                {/* ใช้ Link แทน tag <a href=?> */}
                <Link to="/create">Create</Link>
            </li>
            <li>
                <Link to="/lists">Lists</Link>
            </li>
        </ui>
    )
}

export default Navbar
