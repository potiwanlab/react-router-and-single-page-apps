import { useState } from "react"
import { Route, Routes } from "react-router-dom"
import Create from "./pages/create"
import Home from "./pages/home"
import Lists from "./pages/lists"

const NoMatch= () => <div>404 Not Found</div>

const Main = () => {
    const [skill, setSkill] = useState(["html", "javascript", "css"])

    return (
        <Routes>
            <Route path = "/" index element = {<Home />} />
            <Route path = "/create" element = {<Create 
                skill = {skill}
                setSkill = {setSkill}/>} />
            <Route path = "/lists" element = {<Lists
                skill = {skill}
                setSkill = {setSkill}
                />} />
            <Route path = '*' element = {<NoMatch/>} />
        </Routes>
    )
}

export default Main
