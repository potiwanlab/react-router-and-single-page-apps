import { useState } from "react";
import { useNavigate } from 'react-router-dom';

const Create = () => {
  const [todoInput, setTodo] = useState();
  const [detailInput, setDetail] = useState();

  const navigate = useNavigate();

  const createData = async (todo, detail) => {
    try {
      const res = await fetch("https://todo.showkhun.co/create", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          todo,
          detail
        })
      });

      if (res.ok) {
        // redirect to Lists
        navigate('/lists')
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div>
        <h1>Create</h1>
        <p>
          ToDo : <input onChange={e => setTodo(e.target.value)} />
        </p>
        <p>
          Detail : <textarea onChange={e => setDetail(e.target.value)} />
        </p>
        <button onClick={() => createData(todoInput, detailInput)}>Add Todo</button>
      </div>
    </>
  );
};

export default Create;
