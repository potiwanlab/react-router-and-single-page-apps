import React, { useState, useEffect } from "react";

const Lists = () => {
  const [showList, setList] = useState();

  useEffect(() => {
    async function fetchData() {
      const result = await fetch("https://todo.showkhun.co/lists");
      result
      .json()
      .then(result => setList(result));
    }
    fetchData();
})

  return <div>
      <h1>Lists</h1>
      <span>{JSON.stringify(showList)}</span>
  </div>
}

export default Lists;
