import './App.css';
import { BrowserRouter } from "react-router-dom";
import Navbar from './components/navbar';
import Main from './main';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navbar />
        <Main />
      </BrowserRouter>
    </div>
  );
}

export default App;
